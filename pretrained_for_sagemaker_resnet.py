#a 50 layer resnet pretrained on chosen weights
from keras.applications.resnet50 import ResNet50
#
from keras.applications.resnet50 import preprocess_input
#Functions for preprocessing images
from keras.preprocessing import image
#initialize a NN by a sequence of layers
from keras.models import Sequential
#to perform convolution on images, first step of a CNN
from keras.layers import Conv2D
#Pooling, second step of a CNN
#Apply averaging filter to non-overlapping regions to take average of sub-region
#To create a new output matrix as abstract representation to prevent overfitting
from keras.layers import AveragePooling2D
#Convert all the resultant 2D arrays into a long continuous linear vector
from keras.layers import Flatten
#Used to build fully connected layers
from keras.layers import Dense 

from keras.models import Model, load_model, model_from_json
from keras.optimizers import SGD 
#generator for training
from keras.preprocessing.image import ImageDataGenerator 
#for monitoring model 
from keras.callbacks import EarlyStopping, ModelCheckpoint

#from PIL import Image

import math, os, sys
import numpy as np 

#Globals

METRIC = 'accuracy'
#4 categories: human, dog, cat, empty
NUM_CLASSES = 4
#width and length has to be >= 75
IMG_WIDTH = 720 #720 #360
IMG_LEN = 480 #480 #240


EPOCHS = 25 #8
BATCH_SIZE = 20 #100

#Load pretrained model and prepare it for transfer learning
def loadModel():
    print("Loading Pre-trained ResNet 50-layers...")

    #resnet 50 without top fully connected layers
    resnet50_notop = ResNet50(include_top=False, weights='imagenet',input_tensor=None,pooling = 'avg')
    #0.0001
    learning_rate = 0.0001 
    #lr_decay = learning_rate / EPOCHS 

    #sequence of layers
    model = Sequential()

    #add pretrained weights
    model.add(resnet50_notop)

    #add Dense layer for 4-class classification
    #activation = 'relu' for opt maybe 
    model.add(Dense(NUM_CLASSES, activation = 'softmax'))

    #allow finetuning convolution layer
    #model.layers[0].trainable = True 
    #freeze resnet model's layers
    # for layer in resnet50_notop.layers:
    #     layer.trainable = False
    model.layers[0].trainable = False

    sgd_optimizer = SGD(lr=learning_rate, decay=1e-6, momentum = 0.9, nesterov=True)
    print("SGD info:")
    print("\n")
    print("learning rate:", "\t", learning_rate)

    print("decay:\t1e-6")

    print("momentum:\t0.9")


    model.compile(optimizer = sgd_optimizer, loss = 'categorical_crossentropy', metrics = ['accuracy'] )

    return model


def trainModel(model): 

    list_ = os.listdir('images/train/cat/')
    cat_count = len(list_)
    list_ = os.listdir('images/train/dog/')
    dog_count = len(list_)
    list_ = os.listdir('images/train/human/')
    human_count = len(list_)
    list_ = os.listdir('images/train/nothing/')
    nothing_count = len(list_)
    total_count = cat_count + dog_count + human_count + nothing_count
    train_steps = math.ceil(total_count/BATCH_SIZE)

    list_ = os.listdir('images/eval/cat/')
    cat_count = len(list_)
    list_ = os.listdir('images/eval/dog/')
    dog_count = len(list_)
    list_ = os.listdir('images/eval/human/')
    human_count = len(list_)
    list_ = os.listdir('images/eval/nothing/')
    nothing_count = len(list_)
    total_count = cat_count + dog_count + human_count + nothing_count 
    eval_steps = math.ceil(total_count/BATCH_SIZE)

    #Create image (data) generators for training and testing 

    data_generator = ImageDataGenerator(preprocessing_function=preprocess_input)

    train_generator = data_generator.flow_from_directory('images/train/',target_size = (IMG_WIDTH,IMG_LEN),color_mode = "rgb", batch_size=BATCH_SIZE,class_mode='categorical')
    eval_generator = data_generator.flow_from_directory('images/eval/',target_size = (IMG_WIDTH,IMG_LEN),color_mode = "rgb", batch_size=BATCH_SIZE,class_mode='categorical')
    test_generator = data_generator.flow_from_directory('images/test/', target_size = (IMG_WIDTH,IMG_LEN),color_mode = "rgb", batch_size = BATCH_SIZE,class_mode = None, shuffle = False, seed = 143)


    #to monitor during training process
    early_stopper = EarlyStopping(monitor = 'val_loss', patience = 2)
    checkpointer = ModelCheckpoint(filepath = 'test_weights_sagemaker.h5', monitor = 'val_loss', save_best_only = True, mode = 'auto')

    #training
    print("Training...")
    train_history = model.fit_generator(train_generator, steps_per_epoch=train_steps,epochs=EPOCHS,validation_data=eval_generator, validation_steps= eval_steps,callbacks=[checkpointer,early_stopper] )

    #save weights + architecture
    #model.save_weights('sagemaker_weights.h5')
    with open('sagemaker_serialized_model.json', 'w') as f:
       f.write(model.to_json())
    #save model
    #model.save('pretrained_sagemaker_model.h5')


    return test_generator


def predict(data_generator): 
    #model = loadModel()
    #model.load_weights('sagemaker_weights.h5')
    json_file = open('sagemaker_serialized_model.json', 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    loaded_model = model_from_json(loaded_model_json)
    loaded_model.load_weights("test_weights_sagemaker.h5")

    sgd_optimizer = SGD(lr=0.0001, decay=1e-6, momentum = 0.9, nesterov=True)
    loaded_model.compile(optimizer = sgd_optimizer, loss = 'categorical_crossentropy', metrics = ['accuracy'] )

    #model = load_model('pretrained_sagemaker_model.h5')

    data_generator.reset()
    predicted = loaded_model.predict_generator(data_generator, steps = len(data_generator), verbose = 1)
    print(predicted)
    print("----------------------------------------------------------------------")

    predicted_class = np.argmax(predicted, axis =1)
    print(predicted_class)


def main():

    resnet_model = loadModel()
    test_generator = trainModel(resnet_model)
    #data_generator = ImageDataGenerator(preprocessing_function=preprocess_input)
    #test_generator = data_generator.flow_from_directory('images/test/', target_size = (IMG_WIDTH,IMG_LEN),color_mode = "rgb", batch_size = BATCH_SIZE,class_mode = None, shuffle = False, seed = 143)

    predict(test_generator)

    
main()