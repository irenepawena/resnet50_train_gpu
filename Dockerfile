FROM python:3
#FROM nvcr.io/nvidia/tensorflow:19.01

ADD pretrained_for_sagemaker_resnet.py /

#RUN sudo apt install python3-pip 
RUN pip3 install keras
RUN pip3 install numpy 
RUN pip3 install tensorflow
RUN pip3 install pillow

COPY images .

CMD [ "python3", "pretrained_for_sagemaker_resnet.py"]